package stringslice_test

import (
	"fmt"
	"regexp"
	"strings"
	"testing"

	"md0.org/stringslice"
)

var (
	s0 = []string{"one"}
	s1 = []string{"one", "two", "four"}
	s2 = []string{"one", "two", "three"}
	s3 = []string{"one", "two", "four"}
	s4 = []string{"four", "one", "two"}
)

func TestEqual(t *testing.T) {
	if stringslice.Equal(s0, s1) != false {
		t.Error("[SSE-0]unequal slices reported equal")
	}
	if stringslice.Equal(s1, s2) != false {
		t.Error("[SSE-1]unequal slices reported equal")
	}
	if stringslice.Equal(s1, s3) != true {
		t.Error("[SSE-2]equal slices reported unequal")
	}
	if stringslice.Equal(s1, s4) != false {
		t.Error("[SSE-3]unequal slices reported equal")
	}
	if stringslice.Equal(s3, s4) != false {
		t.Error("[SSE-4]unequal (due to order) slices reported equal")
	}
}

func TestSetEqual(t *testing.T) {
	cc := []struct {
		s    []string
		t    []string
		want bool
	}{
		{[]string{}, []string{}, true},
		{s3, s4, true},
		{s1, s3, true},
		{s1, s3, true},
		{s1, s2, false},
	}

	for _, c := range cc {
		got := stringslice.SetEqual(c.s, c.t)
		if got != c.want {
			t.Errorf("SetEqual(%v, %v) = %v, want %v", c.s, c.t, got, c.want)
		}
	}
}

func TestDifferences(t *testing.T) {
	tests := []struct {
		s        []string
		t        []string
		expected [][]string
	}{
		{[]string{}, []string{}, [][]string{}},
		{s1, s1, [][]string{}},
		{s0, s1, [][]string{[]string{"", "two"}, []string{"", "four"}}},
		{s1, s2, [][]string{[]string{"four", "three"}}},
		{s2, []string{}, emptySecond(s2)},
		{[]string{}, s4, emptyFirst(s4)},
		{s3, s4, zip(s3, s4)},
	}

	for _, test := range tests {
		actual := stringslice.Differences(test.s, test.t)
		if !stringSliceSliceEqual(actual, test.expected) {
			t.Errorf("\ninput s:%v\ninput t:%v\nresult: %v\nwanted: %v",
				test.s, test.t, actual, test.expected)
		}
	}
}

func TestInclude(t *testing.T) {
	tests := []struct {
		input    string
		list     []string
		expected bool
	}{
		{"one", s0, true},
		{"one", []string{}, false},
		{"two", s0, false},
		{"", s1, false},
		{"four", s1, true},
		{"two", s1, true},
		{"one", s1, true},
		{"cats", s1, false},
	}

	for _, test := range tests {
		if stringslice.Include(test.input, test.list) != test.expected {
			t.Errorf("Include(%q, %v) = %v; expected %v",
				test.input, test.list, !test.expected, test.expected)
		}
	}
}

func TestIntersects(t *testing.T) {
	tests := []struct {
		cand     []string
		univ     []string
		expected bool
	}{
		{s0, s0, true},
		{s0, s1, true},
		{s0, s2, true},
		{s0, s3, true},
		{s0, s4, true},
		{s1, s2, true},
		{s1, s3, true},
		{s1, s4, true},
		{[]string{"cat"}, s4, false},

		// Reversing cand & univ should always be the same.
		{s1, s0, true},
		{s2, s0, true},
		{s3, s0, true},
		{s4, s0, true},
		{s2, s1, true},
		{s3, s1, true},
		{s4, s1, true},
		{s4, []string{"cat"}, false},
	}

	for _, test := range tests {
		if stringslice.Intersects(test.cand, test.univ) != test.expected {
			t.Errorf("Intersects(%v, %v) = %v; expected %v",
				test.cand, test.univ, test.expected, !test.expected)
		}
	}
}

func TestMapFuncTrimSpace(t *testing.T) {
	tests := []struct {
		input    []string
		expected []string
	}{
		{
			[]string{},
			[]string{},
		},
		{
			[]string{
				"cats",
			},
			[]string{
				"cats",
			},
		},
		{
			[]string{
				"  cats",
				"dogs   ",
				"   rabbit   ",
			},
			[]string{
				"cats",
				"dogs",
				"rabbit",
			},
		},
		{
			[]string{
				"\n\tbadger",
				"crocodile\n  \t  ",
				"  \n\t\n  whale\r",
				"lynx",
			},
			[]string{
				"badger",
				"crocodile",
				"whale",
				"lynx",
			},
		},
	}

	for _, test := range tests {
		actual := stringslice.MapFunc(test.input, strings.TrimSpace)
		if !stringslice.Equal(actual, test.expected) {
			t.Errorf("MapFunc(%v) = %v; expected %v", test.input, actual, test.expected)
		}
	}
}

func TestMapFuncCustomFunc(t *testing.T) {
	tests := []struct {
		input    []string
		expected []string
	}{
		{
			[]string{},
			[]string{},
		},
		{
			[]string{
				"cats",
			},
			[]string{
				"stac",
			},
		},
		{
			[]string{
				"stac",
				"sgod",
				"tibbar",
			},
			[]string{
				"cats",
				"dogs",
				"rabbit",
			},
		},
		{
			[]string{
				"badger",
				"crocodile",
				"whale",
				"lynx",
			},
			[]string{
				"regdab",
				"elidocorc",
				"elahw",
				"xnyl",
			},
		},
	}

	for _, test := range tests {
		actual := stringslice.MapFunc(test.input, func(s string) string {
			var b strings.Builder
			for i := len(s) - 1; i >= 0; i-- {
				fmt.Fprintf(&b, "%s", s[i:i+1])
			}
			return b.String()
		})

		if !stringslice.Equal(actual, test.expected) {
			t.Errorf("MapFunc(%v) = %v; expected %v", test.input, actual, test.expected)
		}
	}
}

func TestFilterFuncRemoveOnlyAllCommas(t *testing.T) {
	// f returns true if s does NOT match /^,+$/.
	f := func(s string) bool {
		allCommas, err := regexp.MatchString("^,+", s)
		return !allCommas || err != nil
	}

	tests := []struct {
		input    []string
		expected []string
	}{
		{
			[]string{},
			[]string{},
		},
		{
			[]string{
				",",
			},
			[]string{},
		},
		{
			[]string{
				",",
				",,,,",
			},
			[]string{},
		},
		{
			[]string{
				",",
				",,,,",
				" ,,,,",
			},
			[]string{
				" ,,,,",
			},
		},
		{
			[]string{
				"cats,dogs,rabbits",
				",,,,,,,,",
			},
			[]string{
				"cats,dogs,rabbits",
			},
		},
		{
			[]string{
				"1,2,3",
				",,,,,,,,",
				"4,5,6",
				",",
				"7,8,9,10",
				"11,12",
				",,,,,,",
			},
			[]string{
				"1,2,3",
				"4,5,6",
				"7,8,9,10",
				"11,12",
			},
		},
	}

	for _, test := range tests {
		actual := stringslice.FilterFunc(test.input, f)
		if !stringslice.Equal(actual, test.expected) {
			t.Errorf("FilterFunc(%v) = %v; expected %v", test.input, actual, test.expected)
		}
	}
}

func TestToByteSliceSlice(t *testing.T) {
	res := stringslice.ToByteSliceSlice(s2)
	for i := range res {
		if string(res[i]) != s2[i] {
			t.Errorf("ToByteSliceSlice on %s: got %s", s2[i], string(res[i]))
		}
	}

	res = stringslice.ToByteSliceSlice([]string{})
	if len(res) != 0 {
		t.Errorf("ToByteSliceSlice: converting empty slice nonempty?")
	}

	res = stringslice.ToByteSliceSlice(nil)
	if res != nil {
		t.Errorf("ToByteSliceSlice: converting nil slice not nil")
	}
}

func TestAllEqual(t *testing.T) {
	cc := []struct {
		ss   []string
		want bool
	}{
		{
			[]string{},
			true,
		},
		{
			[]string{"cat"},
			true,
		},
		{
			[]string{"cat", "cat"},
			true,
		},
		{
			[]string{"cat", "cat", "cat", "cat", "cat", "cat", "cat", "cat", "cat"},
			true,
		},
		{
			[]string{"cat", "dog"},
			false,
		},
		{
			[]string{"cat", ""},
			false,
		},
	}

	for _, c := range cc {
		got := stringslice.AllEqual(c.ss)
		if got != c.want {
			t.Errorf("AllEqual(%v) = %v; want %v", c.ss, got, c.want)
		}
	}
}

func TestAppendToAll(t *testing.T) {
	cc := []struct {
		in    [][]string
		toAdd string
	}{
		{
			[][]string{
				[]string{"a", "b", "c"},
				[]string{"d", "e"},
			},
			"cat",
		},
	}

	for _, c := range cc {
		got := stringslice.AppendToAll(c.toAdd, c.in)
		if len(c.in) != len(got) {
			t.Errorf("TestAppendToAll: mismatched slice-of-slice len: c.in: %v, got: %v", c.in, got)
		}
		for i, e := range c.in {
			if len(e)+1 != len(got[i]) {
				t.Errorf("TestAppendToAll: mismatched element len, input: %v, output: %v", e, got[i])
			}
		}
		for _, e := range got {
			if e[len(e)-1] != c.toAdd {
				t.Errorf("TestAppendToAll: last elt not equal to %q", c.toAdd)
			}
		}
	}
}

func TestIncludeFunc(t *testing.T) {
	cc := []struct {
		in   []string
		fn   func(string) bool
		want bool
	}{
		{
			[]string{"cat:circle", "dog:hazel"},
			func(s string) bool { return strings.HasSuffix(s, "circle") },
			true,
		},
		{
			[]string{"cat:circle", "dog:hazel"},
			func(s string) bool { return strings.HasPrefix(s, "circle") },
			false,
		},
	}

	for _, c := range cc {
		got := stringslice.IncludeFunc(c.in, c.fn)
		if got != c.want {
			t.Errorf("IncludeFunc(%v) = %v, want %v", c.in, got, c.want)
		}
	}
}

func TestIntersection(t *testing.T) {
	ss := func(s ...string) []string {
		return s
	}
	cc := []struct {
		s    []string
		t    []string
		want []string
	}{
		{
			ss(),
			ss(),
			ss(),
		},

		// disjoint
		{
			ss("a", "b", "c"),
			ss("d", "e", "f"),
			ss(),
		},

		// disjoint with empty; empty as t, empty as s
		{
			ss("eros", "gers", "pega", "argo"),
			ss(),
			ss(),
		},
		{
			ss(),
			ss("eros", "gers", "pega", "argo"),
			ss(),
		},

		// equal
		{
			ss("eros", "gers", "pega", "argo"),
			ss("eros", "gers", "pega", "argo"),
			ss("argo", "eros", "gers", "pega"),
		},

		// equal but different order
		{
			ss("eros", "gers", "pega", "argo"),
			ss("pega", "eros", "argo", "gers"),
			ss("argo", "eros", "gers", "pega"),
		},

		// have intersection, reversing order of s & t
		{
			ss("pact", "carp", "inbe", "noma", "polo", "late", "derm"),
			ss("mowt", "inbe", "polo", "lehr", "hure", "noma", "toro"),
			ss("inbe", "noma", "polo"),
		},
		{
			ss("mowt", "inbe", "polo", "lehr", "hure", "noma", "toro"),
			ss("pact", "carp", "inbe", "noma", "polo", "late", "derm"),
			ss("inbe", "noma", "polo"),
		},

		// how does it deal with duplicates?
		// (A: as expected, I guess)
		// duplicate only in s
		{
			ss("toro", "hure", "hure", "amah", "lucy"),
			ss("derm", "hure", "lucy", "gong"),
			ss("hure", "lucy"),
		},
		// matching dups in s & t
		{
			ss("toro", "hure", "hure", "amah", "lucy"),
			ss("derm", "hure", "lucy", "gong", "hure"),
			ss("hure", "hure", "lucy"),
		},
		{
			ss("pact", "pact", "pact", "pact", "pact"),
			ss("pact", "pact", "pact"),
			ss("pact", "pact", "pact"),
		},

		// end
	}

	for _, c := range cc {
		got := stringslice.Intersection(c.s, c.t)
		if !stringslice.Equal(got, c.want) {
			t.Errorf("Intersection(%v, %v) = %v, want %v", c.s, c.t, got, c.want)
		}
	}
}

func TestMinus(t *testing.T) {
	ss := func(s ...string) []string {
		return s
	}
	cc := []struct {
		s    []string
		t    []string
		want []string
	}{
		// extreme basics
		{
			ss(),
			ss(),
			ss(),
		},
		{
			ss(),
			ss("dog", "cat", "mole", "possum"),
			ss(),
		},
		// (this is the archetype for the "take every 2 combo,
		// every 3 combo" below)
		{
			ss("dog", "cat", "mole", "possum"),
			ss(),
			ss("cat", "dog", "mole", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "dog", "mole", "possum"),
			ss(),
		},

		// vanilla basics
		{
			ss("dog", "cat"),
			ss("cat"),
			ss("dog"),
		},
		{
			ss("dog", "cat"),
			ss("dog"),
			ss("cat"),
		},

		// take away every element of s, one at a time
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat"),
			ss("dog", "mole", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("dog"),
			ss("cat", "mole", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("mole"),
			ss("cat", "dog", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("possum"),
			ss("cat", "dog", "mole"),
		},

		// take away every combo of two elements from s
		{
			ss("dog", "cat", "mole", "possum"),
			ss("mole", "possum"),
			ss("cat", "dog"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "possum"),
			ss("dog", "mole"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "mole"),
			ss("dog", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("dog", "possum"),
			ss("cat", "mole"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("dog", "mole"),
			ss("cat", "possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "dog"),
			ss("mole", "possum"),
		},

		// take away every combo of three
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "dog", "mole"),
			ss("possum"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("dog", "cat", "possum"),
			ss("mole"),
		},
		{
			ss("dog", "cat", "mole", "possum"),
			ss("cat", "mole", "possum"),
			ss("dog"),
		},
	}

	for _, c := range cc {
		got := stringslice.Minus(c.s, c.t)
		if !stringslice.Equal(got, c.want) {
			t.Errorf("Minus(%v, %v) = %v, want %v", c.s, c.t, got, c.want)
		}
	}
}

// Given [1 2]
// Return [["" 1] ["" 2]]
func emptyFirst(s []string) [][]string {
	var res [][]string
	for _, e := range s {
		res = append(res, []string{"", e})
	}
	return res
}

// Given [1 2]
// Return [[1 ""] [2 ""]]
func emptySecond(s []string) [][]string {
	var res [][]string
	for _, e := range s {
		res = append(res, []string{e, ""})
	}
	return res
}

// Given [1 2] [3 4]
// Return [[1 3] [2 4]]
func zip(s, t []string) [][]string {
	var res [][]string
	if len(s) != len(t) {
		return res
	}
	for i := range s {
		res = append(res, []string{s[i], t[i]})
	}
	return res
}

// Compare two slices of string slices (meta...)
func stringSliceSliceEqual(s, t [][]string) bool {
	if len(s) != len(t) {
		return false
	}
	for i, e := range s {
		if !stringslice.Equal(e, t[i]) {
			return false
		}
	}
	return true
}
