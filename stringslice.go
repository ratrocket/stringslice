package stringslice

import "sort"

// Equal tests the equality of two string slices.
func Equal(s, t []string) bool {
	if len(s) != len(t) {
		return false
	}
	for i, e := range s {
		if e != t[i] {
			return false
		}
	}
	return true
}

// SetEqual (there's certainly a better name...) compares s and t
// without respect to the order of their elements.  It's really just a
// convenience to allow the client to not sort s and t prior to calling
// Equal.
//
// Argument slices are *not* modified.
func SetEqual(s, t []string) bool {
	// Before incurring cost of copying, try the simple false case.
	if len(s) != len(t) {
		return false
	}
	// Empty sets are equal.
	if len(s) == 0 {
		return true
	}
	s0 := make([]string, len(s))
	t0 := make([]string, len(t))
	copy(s0, s)
	copy(t0, t)
	sort.Strings(s0)
	sort.Strings(t0)
	return Equal(s0, t0)
}

// Differences returns a slice of string slices showing which strings
// differ between s and t.  (Order matters.)
func Differences(s, t []string) [][]string {
	var res [][]string
	var max, i int
	var sLonger bool = false
	var tLonger bool = false

	// "max" is the maximum commonly indexable index.
	if len(s) == len(t) {
		max = len(s)
	} else if len(s) > len(t) {
		max = len(t)
		sLonger = true
	} else { // len(t) > len(s)
		max = len(s)
		tLonger = true
	}

	for i = 0; i < max; i++ {
		if s[i] != t[i] {
			res = append(res, []string{s[i], t[i]})
		}
	}

	if sLonger {
		for ; i < len(s); i++ {
			res = append(res, []string{s[i], ""})
		}
	} else if tLonger {
		for ; i < len(t); i++ {
			res = append(res, []string{"", t[i]})
		}
	}

	return res
}

// Include returns true if ss contains s, false otherwise.
func Include(s string, ss []string) bool {
	for _, t := range ss {
		if t == s {
			return true
		}
	}
	return false
}

func IncludeFunc(ss []string, f func(string) bool) bool {
	for _, s := range ss {
		if f(s) {
			return true
		}
	}
	return false
}

// Intersects returns true if candidates and universe have a non-empty
// intersection.
//
// This is *only* a predicate!  See Intersection for the actual
// intersection.
func Intersects(candidates, universe []string) bool {
	for _, u := range universe {
		for _, c := range candidates {
			if u == c {
				return true
			}
		}
	}
	return false
}

// Intersection returns the intersection of s and t.
//
// Since s and t aren't really sets, duplicates could be present.  If
// there is the same duplicated element in s and t, the return value
// will contain those duplicates.  (This seems like expected behavior.)
//
// One quirk: the result will be in sorted order.
func Intersection(s, t []string) []string {
	var (
		i, j int
		r    = []string{}
	)
	sort.Strings(s)
	sort.Strings(t)
	for i < len(s) && j < len(t) {
		if s[i] == t[j] {
			r = append(r, s[i])
			i++
			j++
		} else if s[i] < t[j] {
			i++
		} else {
			j++
		}
	}
	return r
}

// Minus returns s - t in set terms.
//
// s is the minuend
// t is the subtrahend
// r is the difference
// (in arithmetic terms)
//
// Like Intersection, the result will be in sorted order (for better or
// worse).
func Minus(s, t []string) []string {
	var (
		i, j int
		r    = []string{}
	)
	sort.Strings(s)
	sort.Strings(t)
	for i < len(s) && j < len(t) {
		if s[i] == t[j] {
			i++
			j++
		} else if s[i] < t[j] {
			r = append(r, s[i])
			i++
		} else {
			for j < len(t) && s[i] > t[j] {
				j++
			}
		}
	}
	if i < len(s) {
		r = append(r, s[i:]...)
	}
	return r
}

// MapFunc maps each element s_i in ss to f(s_i).
func MapFunc(ss []string, f func(string) string) []string {
	rr := []string{}
	for _, s := range ss {
		rr = append(rr, f(s))
	}
	return rr
}

func FilterFunc(ss []string, f func(string) bool) []string {
	rr := []string{}
	for _, s := range ss {
		if f(s) {
			rr = append(rr, s)
		}
	}
	return rr
}

func ToByteSliceSlice(ss []string) [][]byte {
	if ss == nil {
		return nil
	}
	rr := make([][]byte, len(ss))
	for i, s := range ss {
		rr[i] = []byte(s)
	}
	return rr
}

func AllEqual(ss []string) bool {
	for i := 0; i < len(ss)-1; i++ {
		if ss[i] != ss[i+1] {
			return false
		}
	}
	return true
}

// return sss with s appended to each element of sss.
func AppendToAll(s string, sss [][]string) [][]string {
	ttt := make([][]string, len(sss))
	for i := range sss {
		ttt[i] = make([]string, len(sss[i])+1)
		copy(ttt[i], sss[i])
	}
	for _, t := range ttt {
		t[len(t)-1] = s
	}
	return ttt
}
